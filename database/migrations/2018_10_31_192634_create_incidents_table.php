<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inc');
            $table->string('element')->nullable();
            $table->string('client_name')->nullable();
            $table->integer('client_id')->nullable();
            $table->string('point_de_contact')->nullable();
            $table->integer('point_de_contact_id')->nullable();
            $table->string('statut');
            $table->datetime('ouvert');
            $table->dateTime('ferme')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incidents', function (Blueprint $table) {
            Schema::dropIfExists('incidents');
        });
    }
}
