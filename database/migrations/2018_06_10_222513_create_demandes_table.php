<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ritm');
            $table->text('element');
            $table->integer('element_id')->nullable();
            $table->string('client_name');
            $table->integer('client_id')->nullable();
            $table->string('demandeur_name');
            $table->integer('demandeur_id')->nullable();
            $table->string('statut');
            $table->dateTime('ouvert');
            $table->dateTime('livraison')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandes');
    }
}
