<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActransverseClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actransverse_client', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('actransverse_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->foreign('actransverse_id')->references('id')->on('actransverses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('actransverse_client', function (Blueprint $table) {
            Schema::dropIfExists('actransverse_client');
        });
    }
}
