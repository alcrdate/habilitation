<html>
   <head>
      <title>Ajax Example</title>
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      
   </head>
   
    <body>
    <input type="text" name="search" id="search" class="form-control" placeholder="Search Customer Data" />
        <h2 align="center">Ajax test ☺</h2>   
        <h3 align="center">Total Data : <span id="total_records"></span></h3>
    </input>
    
    <table class="table table-striped table-bordered">
       <thead>
        <tr>
         <th>Element </th>
         <th>Nom</th>
         <th>Ritm</th>
         <th>Ouvert le</th>
         <th>Livraison</th>
        </tr>
       </thead>
       <tbody>

       </tbody>
      </table>
    
      <div class="container" style="width:500px;">  
                <h3 class="text-center">How to insert Radio Button value</h3>  
                <div class="radio">  
                    @for($i = 0; $i < 10; $i++)
                     <input type="radio" name="gender" value="Male_{{ $i }}" />Male {{ $i }}<br />
                    @endfor   
                     <input type="radio" name="gender" value="Female" />Female <br />  
                     <input type="radio" name="gender" value="Other" />Other <br />  
                </div>  
                <div id="result">
                    
                </div>  
        </div>  

    <button type="button" class="btn inactive">test 1</button>
    <button type="button" class="btn inactive">test 2</button>
    <button type="button" class="btn inactive">test 3</button>
   
</body>
   
</html>
    <style>
        .btn{color:white}
        .active{background:green}
        .inactive{background:red}
    </style>
    
    <script type="text/javascript">
        $(document).ready(function(){

            $(document).ready(function(){
            $('.btn').click(function(){
                $('.btn').removeClass('active').addClass('inactive');
                $(this).removeClass('inactive').addClass('active');
                });
            })

            fetch_demande_data();

            function fetch_demande_data(query = ''){
                $.ajax({
                    url : "{{ route('action') }}",
                    method : "GET",
                    data:{query:query},
                    dataType : 'json',
                    success:function(demandes){
                        $('tbody').html(demandes.table_data);
                        $('#total_records').text(demandes.total_data);
                    }
                })
            }
            
            $(document).on('keyup', '#search', function(){
                var query = $(this).val();
                fetch_demande_data(query);
            });
            
            function radio_btn(gender = ''){
                $.ajax({  
                    url:"{{ route('actionRadio') }}",  
                    method:"get",  
                    data:{gender:gender},
                    dataType : 'json',  
                    success:function(output){  
                        $('#result').text(output);  
                    }  
                }); 
            }

            $('input[type="radio"]').click(function(){  
                var gender = $(this).val();  
                radio_btn(gender); 
            })
        });    
    </script>
    