@extends('layouts.master')

<link rel="stylesheet" href="{{ asset('css/demandes/indexStyle.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/css/jquery.dataTables.css')}}">

@section('content')
  @if(Session::has('success'))
	    <div class="alert alert-success">
	        {{ Session::get('success') }}
	        @php
	          Session::forget('success');
	        @endphp
	    </div>
  @endif
  @if(Session::has('already'))
	    <div class="alert alert-warning">
	        {{ Session::get('already') }}
	        @php
	          Session::forget('already');
	        @endphp
	    </div>
  @endif

<section id = 'indicator' class="content-header">
  <h1>
    Incidents
    <small>Liste des incidents</small>
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route ('dashboard')}}"><i class="fa fa-database"></i>Dashboard</a></li>
    <li class="active">Incidents</li>
  </ol>
</section>

    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                  <div class="panel-heading" id= "box_header">
                    Liste des Incidents
                    <div class="box-tools pull-right">
                        <span id="success_message" class="label label-success"></span>
                    <a href="{{route('export')}}">
                      <button type="button" class="btn btn-box-tool" data-toggle="modal" data-toggle="tooltip" title="Excel">
                        <i id = "icons" class="fa fa-table"></i></button>
                    </a>
                    <a href="{{ route('demandes.create')}}">
                      <button type="button" class="btn btn-box-tool" title="Ajouter" data-toggle="tooltip">
                        <i id = "icons" class="fa fa-plus"></i></button>
                    </a>    
                      <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" title="Srap">
                        <i id = "icons" class="fa fa-cubes"></i></button>
                        
                     </div>
                  </div>

                    <div class="panel-body" >
                        
                        <table class="table table-hover" >
                            <thead>
                                <tr>
                                    <th>Element</th>
                                    <th>Nom Client</th>
                                    <th>INC</th>
                                    <th>Ouvert</th>
                                    <!-- <th>Livraison</th> -->
                                    <!-- <th>Statut</th> -->
                                    <th>Changer statut</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form action="{{route('scrap')}}" method='post'>
      {{ csrf_field() }}
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Paste the code source</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <textarea name='textS' style="resize: none;" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <input type="submit" class="btn" id = "box_header"  value='Scarp ☺'>
        </div>
      </div>
    </div>
  </form>
</div>


@endsection

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript" charset="utf8" src="{{ asset('DataTables/js/jquery.dataTables.js') }}"></script>
{{-- <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script> --}}
<script src="{{asset('js/demandes/indexDemande.js')}}"></script>


