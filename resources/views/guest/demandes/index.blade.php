@extends('layouts.master')

<link rel="stylesheet" href="{{ asset('css/demandes/indexStyle.css') }}">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

@section('content')
    
<section id = 'indicator' class="content-header">
    <h1>
      Demandes
      <small>Liste des demandes</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-database"></i>Demandes</a></li>
    </ol>
</section>

<div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                
                <div class="panel-heading" >
                    Liste des demandes
                    <div class="box-tools pull-right">
                    <a href="{{route('exportGuest')}}">
                      <button type="button" class="btn btn-box-tool" data-toggle="modal" data-toggle="tooltip" title="Excel">
                        <i  style = "color: black;"  id = "icons" class="fa fa-table"></i></button>
                    </a>
                  </div>
                </div>  
                <div class="panel-body">
                    <table  class="table table-striped" id="DemandeTable">
                        <thead>
                            <tr>
                                <th>Element</th>
                                <th>Nom Client</th>
                                <th>RITM</th>
                                <th>Ouvert</th>
                                <th>Statut</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            
        </div>
    </div>
</div>

@endsection

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script src="{{asset('js/guest/guestDemandeIndex.js')}}"></script>