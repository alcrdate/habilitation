@extends('layouts.master')

@section('content')

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<link rel="stylesheet" href="{{ asset('css\clients\indexClient.css') }}">

<section id = 'indicator' class="content-header">
    <h1>
      Clients
      <small>Liste des clients</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-user"></i>Clients</a></li>
    </ol>
</section>

<div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Clients</div>

                <div class="panel-body">
                    <table class="table table-hover" id="guestClientTable">
                        <thead>
                            <tr>
                                <th>Active</th>
                                <th>IGG</th>
                                <th>Nom Complet</th>
                                <th>Email</th>
                                <th>Lot</th>
                                <th>Edition</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script src="{{asset('js/guest/guestClientIndex.js')}}"></script>
