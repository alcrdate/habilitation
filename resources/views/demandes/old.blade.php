@extends('layouts.master')

@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
 
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<form action="{{route('scrap')}}" method='post'>
    {{ csrf_field() }}
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Paste the code source</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <textarea name='textS' style="resize: none;" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" class="btn" id = "box_header"  value='Scarp ☺'>
      </div>
    </div>
  </div>
  </form>
</div>

<div class="box">       
        
      <div id = "box_header" class="box-header with-border">
        <div  class="box-title">
        Liste des demandes
        </div>  
        <!-- <button id="leftBtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Scrap
        </button>  
        <a href="{{ route('demandes.create')}}">
        <button id="rightBtn" type="button" class="btn btn-primary">Ajouter</button>
        </a> -->
          <div class="box-tools pull-right">
          <input type="text" name="search" id="search" class="form-control" placeholder="Rechercher ...." />
          
          <a href="{{ route('demandes.create')}}">
            <button type="button" class="btn btn-box-tool" title="Ajouter" data-toggle="tooltip">
              <i id = "icons" class="fa fa-plus"></i></button>
          </a>    
            <button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" title="Srap">
              <i id = "icons" class="fa fa-cubes"></i></button>
          </div>
        <!-- <a href="{{route('users.create')}}"><img style="max-width: 20px;max-height: 20px;float:right;" src="{{ asset('assets/images/logos/add.png') }}" alt=""></a> -->
      </div>
      <div class="panel-body">
      <div id="success_message" class="ajax_response" style="float:right"></div>
        <table class="table table-hover" id = "demandesTable">
        <thead>
          <tr>
            <th scope="col">Element</th>
            <th scope="col">client</th>
            <th scope="col">RITM</th>
            <th scope="col">Ouvrert le </th>
            <th scope="col">Livraison estimee</th>
            <th scope="col">Statuts</th>
          </tr>
        </thead>
      @if(isset($demandes))
        <tbody>
        @foreach ($demandes as $d)
          <tr>
            <td id = "element">{{ $d->element }}</td>
            <td>{{ $d->client_name }}</td>
            <td>{{ $d->ritm }}</td>
            <td>{{ $d->ouvert }}</td>
            <td>{{ $d->livraison }}</td>
            <td></td>
            <div id = 'result'></div>
            <td id = "statusBtn" >
              
              <button id = "btn" type="button" class="btn btn-box-tool" title="Ouvert" data-toggle="tooltip" value = "Ouvert_{{$d->id}}">
                <i  style = "color:gery;" id = "icons" class="fa fa-circle-o"></i>
              </button>
              
              <button id = "btn" type="button" class="btn btn-box-tool" title="Travail en cours" data-toggle="tooltip" value = "Travail en cours_{{$d->id}}">
                <i style = "color:gery;" id = "icons" class="fa fa-spinner"></i>
              </button>

              <button id = "btn" type="button" class="btn btn-box-tool" title="Fermé terminé" data-toggle="tooltip" value = "Terminé_{{$d->id}}">
                <i style = "color:gery;" id = "icons" class="fa fa-check-circle"></i>
              </button>

              <button id = "btn" type="button" class="btn btn-box-tool" title="Rejeté" data-toggle="tooltip" value = "Rejeté_{{$d->id}}">
                <i style = "color:gery;" id = "icons" class="fa fa-times-circle"></i>
              </button>
              
            </td>
          </tr>
        @endforeach
        {!! $demandes->render() !!}
        </tbody>
      @endif  
        </table>
      </div>
    </div>
  <div>
    <table>
      <th>Statut</th>
      <tbody id = "total">
        <tr>

        </tr>
      </tbody>
    </table>
  </div>
    <!-- <div class="panel-body">  
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">RITM</th>
              <th scope="col">client</th>
              <th scope="col">Element</th>
              <th scope="col">Ouvrert le </th>
              <th scope="col">Livraison estimee</th>
              <th scope="col">Statuts</th>
            </tr>
          </thead>
          <tbody id = "tableAjax" >
            <tr>
            </tr>
          </tbody>
        </table>
      </div> -->
    </div>
</div>    
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<script type="text/javascript">

  $(document).ready(function(){

    $(document).ready(function(){
      $('#demandesTable').DataTable({
        "processing" : true,
        "serverSide" : true,
        "ajax" : "{{ route('demandeTable') }}",
        "columnDefs" : [{
          targets:-1,
          //"visible": false,
          render : function(data,type,row){
            if(type === 'display'){
              if(data === 'Ouvert'){
                  data = '<span class="label label-warning">Ouvert</span>';
                }else if(data === 'Travail en cours'){
                  data = '<a href = "' + data + '" ><span class="label label-primary">Travail en cours</span></a>';
                }else if(data === 'Rejeté'){
                  data = '<span class="label label-danger">Rejeté</span>';
                }else{
                  data = '<span class="label label-success">Terminé</span>';
                }
              }
            return data;
          }
        },
        {
          targets:0,
          render: function ( data, type, row ) {
            return data.substr( 0, 20 ) + ' .... ';
          }
        }],
        "columns" : [
          { 
            "data" : "element", 
            "className" : "dtails-control"
          },
          { "data" : "client_name" },
          { "data" : "ritm" },
          { "data" : "ouvert"},
          { "data" : "livraison"},
          { "data" : "statut"}
        ],
        "order": [[1, 'asc']]
      });
    });
    $('#demandesTable tbody').on('click', 'td.details-control', function () {
        alert('Hello zorld');
    } );

    function demande_data(query = ''){
      $.ajax({
        url : "{{ route('action')}}",
        method : "GET",
        data : {query:query},
        dataType : 'json',
        success : function(demandes){
          $('#tableAjax').html(demandes.table_data);
          $('#total').html(demandes.statut);
        }
      })
    }

    $(document).ready(function(){
      setInterval(demande_data,1000);
    });

    $(document).on('keyup' , '#search' , function(){
      var query = $(this).val();
      demande_data(query);
    });

    function btn_status(status = ''){
      $.ajax({  
          url:"{{ route('changerStatus') }}",  
          method:"get",  
          data:{status:status},
          dataType : 'json',  
          success:function(output){  
              $('#success_message').fadeIn("slow").text(output);
              setTimeout(function() {
                $('#success_message').fadeOut("slow");
              }, 2000 );
          }  
      }); 
    }
    
    $('button').click(function(){  
        var status = $(this).val();  
        btn_status(status); 
    })
  });

  
</script>

<style type="text/css">
  #leftBtn {
    width: 100px; 
    height: 40px; 
    float: right;
    margin-left: 10px;
  }

  #rightBtn{
    width: 100px; 
    height: 40px; 
    float: right;
  }

  #element{
    max-width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  #icons{
    color:white;
  }

  #box_header{
    background-color:#605ca8; 
    color:white;
  }

  #statusBtn i{
    color:grey;
    font-size : 14pt;
  }

  #search{
    margin-right:10px;
    height:29px;
    width: 250px;
    float:left; 
  }

  #success_message{
    background: #CCF5CC;
  }

.ajax_response {
    padding: 10px 20px;
    border: 0;
    display: inline-block;
    margin-top: 20px;
    cursor: pointer;
    display:none;
    color:#555;
}

</style>

