        <div style = "margin-top: 20px;" class="form-group col-md-12">   
            <label style="margin-right:10px;">
                Nombre d'outils : 
            </label>
            <label class="radio-inline">
                <input type="radio" name="applications" value = "application1" checked>1 Outil
            </label>

            <label class="radio-inline">
                <input type="radio" name="applications" value = "application2">2 Outils
            </label>

            <label class="radio-inline">
                <input type="radio" name="applications" value = "application3">3 Outils
            </label>
            
            <label class="radio-inline">
                <input type="radio" name="applications" value = "application4">4 Outils
            
            </label>
            <label class="radio-inline">
                <input type="radio" name="applications" value = "application5">5 Outils
            
            </label>
        </div>
        
            <div id = 'app1'  class="form-group col-md-3">
                <label for="#badge">
                    Outil 1 :     
                </label>    
                <select class="form-control" name = "outil1">
                    @foreach($t['outils'] as $o)
                        <option value="{{$o->id}}">{{$o->nom}}</option>
                    @endforeach 
                </select>
            </div>

            <div hidden id = 'app2' class="form-group col-md-3">
                <label for="#badge">
                    Outil 2 :     
                </label>    
                <select name="outil2" class="form-control">
                        <option selected disabled>Liste des applications</option>
                    @foreach($t['outils'] as $o)
                        <option id = "app2opt" value="{{$o->id}}">{{$o->nom}}</option>
                    @endforeach 
                </select>
            </div>
            <div hidden id = 'app3'  class="form-group col-md-3">
                <label for="#badge">
                    Outil 3 :     
                </label>    
                <select name="outil3" class="form-control" name="badge" id="badge">
                        <option selected disabled>Liste des applications</option>
                    @foreach($t['outils'] as $o)
                        <option value="{{$o->id}}">{{$o->nom}}</option>
                    @endforeach    
                </select>
            </div>

            <div hidden id = 'app4'  class="form-group col-md-3">
                <label for="#badge">
                    Outil 4 :     
                </label>    
                <select name="outil4" class="form-control" name="badge" id="badge">
                        <option selected disabled>Liste des applications</option>
                    @foreach($t['outils'] as $o)
                        <option value="{{$o->id}}">{{$o->nom}}</option>
                    @endforeach
                </select>
            </div>

            <div hidden id = 'app5'  class="form-group col-md-3">
                <label for="#badge">
                    Outil 5 :     
                </label>    
                <select name="outil5" class="form-control" name="badge" id="badge">
                        <option selected disabled>Liste des applications</option>
                    @foreach($t['outils'] as $o)
                        <option value="{{$o->id}}">{{$o->nom}}</option>
                    @endforeach
                </select>
            </div>

    <script>
        $('input[type=radio][name=applications]').change(function(){
            if(this.value == "application1"){
                $('#app1').show();
                $('#app2').hide();
                $('#app3').hide();
                $('#app4').hide();
                $('#app5').hide();

            }else if(this.value == 'application2'){
                $('#app1').show();
                $('#app2').show();
                $('#app3').hide();
                $('#app4').hide();
                $('#app5').hide();

            }else if(this.value == 'application3'){
                $('#app1').show();
                $('#app2').show();
                $('#app3').show();
                $('#app4').hide();
                $('#app5').hide();

            }else if(this.value == 'application4'){
                $('#app1').show();
                $('#app2').show();
                $('#app3').show();
                $('#app4').show();
                $('#app5').hide();

            }else if(this.value == 'application5'){
                $('#app1').show();
                $('#app2').show();
                $('#app3').show();
                $('#app4').show();
                $('#app5').show();
            }
        });
    </script>
