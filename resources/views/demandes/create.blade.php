@extends('layouts.master')
@section('content')

  @if(Session::has('success'))
	    <div class="alert alert-success">
	        {{ Session::get('success') }}
	        @php
	          Session::forget('success');
	        @endphp
	    </div>
  @endif
  @if(Session::has('already'))
	    <div class="alert alert-warning">
	        {{ Session::get('already') }}
	        @php
	          Session::forget('already');
	        @endphp
	    </div>
  @endif

    @php 
      $t = Session::get('t');
    @endphp

@if(empty($t))
  <div class="box ">
      <div style = "background-color:#605ca8; color:white;" class="box-header with-border">
          <h3 class="box-title">Ajouter une demande RITM</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
      </div>

        <div class="box-body">
          <form action="{{ route('demandes.store')}}" method="post">
            {{ csrf_field() }}
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">Element</label>
              
                <input name = "element" type="text" class="form-control" id="email" placeholder="Element" >
             
              @if ($errors->has('element'))
                    <span class="text-danger">{{ $errors->first('element') }}</span>
              @endif

            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">Ouvert Par</label>

              
              <input name = "demandeur_name" type="text" class="form-control" id="inputPassword4" placeholder="Ouvert Par">
            

              @if ($errors->has('demandeur_name'))

                    <span class="text-danger">{{ $errors->first('demandeur_name') }}</span>

              @endif
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputCity">Client</label>
             
                <input name = "client_name" type="text" class="form-control" id="inputCity" placeholder="Client" autocomplete="off">
              
                

              @if ($errors->has('client_name'))

                    <span class="text-danger">{{ $errors->first('client_name') }}</span>

              @endif
            </div>

            <div class="form-group col-md-6">
              <label for="inputState">RITM</label>
              
                <input name = "ritm" type="text" class="form-control" id="inputCity" placeholder="RITM">
              
              @if ($errors->has('ritm'))
                    <span class="text-danger">{{ $errors->first('ritm') }}</span>
              @endif
              
            </div>

            <div class="form-group col-md-3">

              <label for="inputState">Ouvert</label>
              
                <input name = "ouvert" type="text" class="form-control" id="inputCity" placeholder="Ouvert">
              
              @if ($errors->has('ouvert'))
                    <span class="text-danger">{{ $errors->first('ouvert') }}</span>
              @endif
            </div>
            <div class="form-group col-md-3">

              <label for="inputState">Livraison</label>
              
                <input name = "livraison" type="text" class="form-control" id="inputCity" placeholder="Livrasion">
              
              @if ($errors->has('livraison'))
                    <span class="text-danger">{{ $errors->first('livraison') }}</span>
              @endif
              
            </div>
            <div class="form-group col-md-6">

              <label for="inputState">Statut</label>
              
                <input name = "statut" type="text" class="form-control" id="inputCity" placeholder="Status">
              
              @if ($errors->has('statut'))
                    <span class="text-danger">{{ $errors->first('statut') }}</span>
              @endif
              
            </div>

            <div class="form-group col-md-12">

                <button style="float:right;background-color:#272F32;" type="submit" class="btn btn-primary">ADD</button>

            </div>
          </div>
        </form>
        </div>
      </div>
  </div>
@else
        <div class="box ">
          <div style = "background-color:#f5f5f5;" class="box-header with-border">
            <h3 style = "color:black;" class="box-title">Ajouter une demande RITM (Scrapé)</h3>
          </div>

        <div class="box-body">
          <form action="{{ route('demandes.store')}}" method="post">
            {{ csrf_field() }}
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputEmail4">Element</label>
              
            <input readonly value="{{$t['e']}}" name = "element" type="text" class="form-control" id="email" placeholder="Element" >
             
              @if ($errors->has('element'))
                    <span class="text-danger">{{ $errors->first('element') }}</span>
              @endif

            </div>
            <div class="form-group col-md-6">
              <label for="inputPassword4">Ouvert Par</label>

              <input readonly value="{{ Auth::user()->name }}" name = "demandeur_name" type="text" class="form-control" id="inputPassword4" placeholder="Ouvert Par">
              

              @if ($errors->has('demandeur_name'))

                    <span class="text-danger">{{ $errors->first('demandeur_name') }}</span>

              @endif
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputCity">Client</label>
              
              @if($t['e'] == "[MOB] Création / Modification d'un IGG Externe (IET)")
                 <input value="" name = "client_name" type="text" class="form-control" id="inputCity" placeholder="Client" autocomplete="off">   
              @else
                <input readonly value="{{$t['c']}}" name = "client_name" type="text" class="form-control" id="inputCity" placeholder="Client" autocomplete="off">        
              @endif  

              @if ($errors->has('client_name'))

                    <span class="text-danger">{{ $errors->first('client_name') }}</span>

              @endif
            </div>

            <div class="form-group col-md-6">
              <label for="inputState">RITM</label>
              
                <input readonly value="{{ $t['r'] }}" name = "ritm" type="text" class="form-control" id="inputCity" placeholder="RITM">
              
              @if ($errors->has('ritm'))
                    <span class="text-danger">{{ $errors->first('ritm') }}</span>
              @endif
              
            </div>

            <div class="form-group col-md-3">

              <label for="inputState">Ouvert</label>
              
                <input readonly value="{{ $t['o'] }}" name = "ouvert" type="text" class="form-control" id="inputCity" placeholder="Ouvert">
              
              @if ($errors->has('ouvert'))
                    <span class="text-danger">{{ $errors->first('ouvert') }}</span>
              @endif
            </div>
            <div class="form-group col-md-3">

              <label for="inputState">Livraison</label>
              
                <input readonly value="{{ $t['l'] }}" name = "livraison" type="text" class="form-control" id="inputCity" placeholder="Livraison">
              
              @if ($errors->has('livraison'))
                    <span class="text-danger">{{ $errors->first('livraison') }}</span>
              @endif
              
            </div>
            <div class="form-group col-md-6">

              <label for="inputState">Statut</label>
              
                <input readonly value="{{ $t['s'] }}" name = "statut" type="text" class="form-control" id="inputCity" placeholder="Status">
              
              @if ($errors->has('statut'))
                    <span class="text-danger">{{ $errors->first('statut') }}</span>
              @endif
              
            </div>
            
            @if($t['e'] == "TARA - Ajout / Suppression d'accès à une application du catalogue")
              <div>
                @include('demandes.installation')
              </div>
            @endif

            <div class="form-group col-md-12">

                <button style="float:right;background-color:#272F32;" type="submit" class="btn btn-primary">ADD</button>

            </div>
          </div>
        </form>
        </div>
      </div>
      
      
      <div class="box ">
            <div style = "background-color:#f5f5f5;" class="box-header with-border">
                <h3 class="box-title">Email a envoyé</h3>
            </div>

            <div class="box-body">
              <textarea class="form-control" id = "mail" rows="6">{{ $t['m'] }}</textarea>
              <button onclick = "copy()" style="background-color:#272F32;margin-top : 10px;" class = "btn btn-primary" id = "copy">Copier</button>
              <button disabled style="background-color:#272F32;margin-top : 10px;" class = "btn btn-primary" id = "send_mail">Envoyer</button>
            </div>
      </div>

  </div>


@endif


@endsection

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="{{asset('js/demandes/indexDemande.js')}}"></script>


