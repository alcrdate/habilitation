<div>
    <div style = "padding: 20;" class="user-panel">
        <div class="pull-left image">
        <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <p>{{ Auth::user()->igg }}</p>
        @if(Auth::user()->role->role == 'admin')
            <a href="#"><i class="fa fa-circle text-success"></i>ADMIN</a>
        @else
            <a href="#"><i class="fa fa-circle text-success"></i>GUEST</a>
        @endif
        </div>
    </div>
@if(Auth::user()->role->role == 'admin')

    <ul class="sidebar-menu">
        <li class="header"></li>

        <li id = 'navbar'>
        <a href="{{ route('admin') }}">
            <i class="fa fa-area-chart"></i> <span>Admin</span>
        </a>
        </li>

        <li id = 'navbar'>
        <a href="{{ route('dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
        </li>
        
        <li id = 'navbar'>
        <a href="{{ route('demandes.index') }}">
            <i class="fa fa-database"></i> <span>Demandes</span>
        </a>
        </li>

        <li id = 'navbar'>
            <a href="{{ route('incidents.index') }}">
                <i class="fa fa-warning"></i> <span>Incidents</span>
            </a>
        </li>


        <li id = 'navbar'>
        <a href="{{ route('clients.index') }}">
            <i class="fa fa-user"></i> <span>Clients</span>
        </a>
        </li>
    </ul>
@else
 <ul class="sidebar-menu">
        <li class="header"></li>

        <li id = 'navbar'>
            <a href="{{route('guestDemandesIndex')}}">
                <i class="fa fa-database"></i> <span>Demandes</span>
            </a>
        </li>

        <li id = 'navbar'>
            <a href="{{ route('guestClientsIndex') }}">
                <i class="fa fa-user"></i> <span>Clients</span>
            </a>
        </li>
    </ul>
@endif
</div>

<style type="text/css">
    #navbar a {
        color: #272F32;
    }

    a {
        /*color: #dd4b39;*/
        color : #272F32;
    }
</style>