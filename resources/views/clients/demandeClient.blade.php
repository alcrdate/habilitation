@extends('layouts.master')
<script src="https://unpkg.com/ag-grid/dist/ag-grid.min.noStyle.js"></script>
<link rel="stylesheet" href="{{ asset('css/clients/clientDemandes.css') }}">
@section('content')
    
    <div class="box">
        <div id = 'box_header' class="box-header with-border">
          <h3 class="box-title">{{$client->nom_complet}} | {{ $client->lot_name }}</h3>

          <div class="box-tools pull-right">
            <button data-toggle="collapse" data-target="#collapseModifier" type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
      </div>

      <div id = "collapseModifier" class = "collapse in box-body">
      
            @if($client->active == 'active')
                <div class="form-group col-md-12">
                        <button value = "{{$client->id}}" id = "btnDesactive" class="btn btn-lg btn-danger">Desactive !</button>
                </div>
                @else
                <div class="form-group col-md-12">
                        <button value = "{{$client->id}}" id = "btnActive" class="btn btn-lg btn-success">Activer</button>
                </div>
            @endif

            <div id="success_message" class="ajax_response alert alert-success" style ="display:none;" role = "alert"></div>
            
            <div id="desactive_message" class="ajax_response alert alert-danger" style ="display:none;" role = "alert"></div>

      <form action="{{ route('clients.edit' , $client) }}" method="get" autocomplete = "off">
            {{ csrf_field() }}
          <div class="form-row">
        @if(empty($client->igg))
            <div class="form-group has-warning col-md-3">
        @else
            <div class="form-group col-md-3">
        @endif
              <label>IGG</label>
              
                <input value = "{{$client->igg}}" name = "igg" type="text" class="form-control" id="igg" placeholder="IGG" >
             
              @if ($errors->has('igg'))
                    <span class="text-danger">{{ $errors->first('igg') }}</span>
              @endif

            </div>
        
        @if(empty($client->nom_complet))
            <div class="form-group has-warning col-md-3">
        @else
            <div class="form-group col-md-3">
        @endif
              <label>Nom complet</label>
              <input value = "{{$client->nom_complet}}" name = "nom_complet" type="text" class="form-control" id="nom_complet" placeholder="Nom complet">
              @if ($errors->has('nom_complet'))
                    <span class="text-danger">{{ $errors->first('nom_complet') }}</span>
              @endif
            </div>
          </div>
          <div class="form-row">
        @if(empty($client->email))
            <div class="form-group has-warning col-md-6">
        @else
            <div class="form-group col-md-6">
        @endif       
              <label>Email</label>
                <input value = "{{$client->email}}" name = "email" type="text" class="form-control" id="email" placeholder="Email">

              @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
              @endif
            </div>

            <div class="form-group col-md-3">
              <label for="inputState">Badge Total</label>

              <select class="form-control" name="badge" id="badge">
                @if($client->badge_total == 0)
                    <option value="1">Avec</option>
                    <option selected value="0">Sans</option>
                @else
                    <option selected value="1">Avec</option>
                    <option value="0">Sans</option>
                @endif
              </select>
              
              @if ($errors->has('badge'))
                    <span class="text-danger">{{ $errors->first('badge') }}</span>
              @endif  
            </div>

            <div class="form-group col-md-3">
              <label for="inputState">Admin</label>

              <select class="form-control " name="admin" id="admin">
                @if($client->admin == 0)
                    <option value="1">Avec</option>
                    <option selected value="0">Sans</option>
                @else
                    <option selected value="1">Avec</option>
                    <option value="0">Sans</option>
                @endif
              </select>
              
              @if ($errors->has('admin'))
                    <span class="text-danger">{{ $errors->first('admin') }}</span>
              @endif  
            </div>

            <div class="form-group col-md-3">
              <label for="inputState">Internet</label>

              <select class="form-control" name="internet" id="internet">
                @if($client->net == 0)
                    <option value="1">Avec</option>
                    <option selected value="0">Sans</option>
                @else
                    <option selected value="1">Avec</option>
                    <option value="0">Sans</option>
                @endif
              </select>
              
              @if ($errors->has('internet'))
                    <span class="text-danger">{{ $errors->first('internet') }}</span>
              @endif  
            </div>

            <div class="form-group col-md-3">
              <label for="inputState">Lot</label>

              <select class="form-control" name = "lot">
                @if($client->lot->nom == '' || $client->lot->nom == 'pas de lot')
                    <option selected value="11">Pas de lot</option>
                    @foreach($lots as $l)
                        <option value="{{ $l->id }}" name ="lot" >{{ $l->nom }}</option>
                    @endforeach
                @else    
                    <option selected value="{{$client->lot_id}}">{{$client->lot->nom}}</option>
                    @foreach($lots as $l)
                        <option value="{{ $l->id }}" name ="lot">{{ $l->nom }}</option>
                    @endforeach
                @endif    
              </select>
              
              @if ($errors->has('lot'))
                    <span class="text-danger">{{ $errors->first('lot') }}</span>
              @endif  
            </div>
            
        @if(empty($client->al))
            <div class="form-group has-warning col-md-3">
        @else
            <div class="form-group col-md-3">
        @endif 
              <label>Compte AL</label>
              <input value = "{{$client->al}}" name = "al" type="text" class="form-control" id="al" placeholder="Compte AL">
              @if ($errors->has('al'))
                    <span class="text-danger">{{ $errors->first('al') }}</span>
              @endif
            </div>
        
        @if(empty($client->vm))
            <div class="form-group has-warning col-md-3">
        @else
            <div class="form-group col-md-3">
        @endif 

              <label>Nom VM</label>
              <input value = "{{$client->vm}}" name = "vm" type="text" class="form-control" id="vm" placeholder="Nom VM">
              @if ($errors->has('vm'))
                    <span class="text-danger">{{ $errors->first('vm') }}</span>
              @endif
            </div>
            
            <div class="form-group col-md-12">
                <button id ="btn" type="submit" class="btn btn-primary">Modifier</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    
    <div class="box">
            <div id = 'box_header' class="box-header with-border">
                <h3 class="box-title">Les dernières demandes</h3>

                <div class="box-tools pull-right">
                    <button data-toggle="collapse" data-target="#collapseDemande" type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div id = "collapseDemande" class="collapse box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">RITM</th>
                        <th scope="col">Element</th>
                        <th scope="col">Statut</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($client->demande as $d)
                        <tr>
                            <td>{{ $d->ritm}}</td>
                            <td>{{ $d->element}}</td>
                            <td>{{ $d->statut}}</td>
                        </tr>
                    @endforeach      
                    </tbody>
                </table>    
            </div>
    </div>

        <div class="box ">
            <div id = 'box_header' class="box-header with-border">
                <h3 class="box-title">Liste des applications (par lot)</h3>

                <div class="box-tools pull-right">
                    <button data-toggle = "collapse" data-target = "#collapseApplication" type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div id = "collapseApplication" class="collapse box-body">
                <table class="table table-hover">
                    <thead>
                        @if(isset($lot))
                            @foreach($lot->applications_lot as $app)
                                <th>{{ $app->nom }}</th>
                            @endforeach
                           
                    </thead>
                    <tbody>
                            @foreach($lot->applications_lot as $app)
                                <td id = "na">N/A</td>
                            @endforeach
                    </tbody>
                        @endif 
                </table>    
            </div>
        </div>

        <div class="box ">
            <div id = 'box_header' class="box-header with-border">
                <h3 class="box-title">Liste des applications</h3>

                <div class="box-tools pull-right">
                    <button data-toggle = "collapse" data-target = "#collapseApplication2" type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div id = "collapseApplication2" class="collapse box-body">
                <table class="table table-hover">
                    <tbody>
                            @foreach($client->applications as $app)
                            <tr>
                                <td> - {{ $app->nom }}</td>
                            </tr>
                            @endforeach
                           
                    </tbody>
                </table>    
            </div>
        </div> 


    <div class="box ">
            <div id = 'box_header' class="box-header with-border">
                <h3 class="box-title">Accès transverses</h3>

                <div class="box-tools pull-right">
                    <button data-toggle ="collapse" data-target = "#collapseAcces" type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div id = "collapseAcces" class="collapse box-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Badge total</th>
                        <th scope="col">IET</th>
                        <th scope="col">Droits admin</th>
                        <th scope="col">AL#</th>
                        <th scope="col">Net</th>
                        <th scope="col">VM#</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr >
                            @if($client->badge_total == 0)<td id = "na">N/A</td>@else<td id = "ok">OK</td>@endif
                            @if($client->iet == 0)<td id = "na">N/A</td>@else<td id = "ok">OK</td>@endif
                            @if($client->admin == 0)<td id = "na">N/A</td>@else<td id = "ok">OK</td>@endif
                            @if($client->al == "")<td id = "na">N/A</td>@else<td id = "ok">{{$client->al}}</td>@endif
                            @if($client->net == 0)<td id = "na">N/A</td>@else<td id = "ok">OK</td>@endif
                            @if($client->vm == "")<td id = "na">N/A</td>@else<td id = "ok">{{$client->vm}}</td>@endif
                        </tr>
                    </tbody>
                </table>    
            </div>
    </div>


@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript" src="{{asset('js/clients/demandeclient.js')}}"></script>
