@extends('layouts.master')

@section('content')

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<link rel="stylesheet" href="{{ asset('css\dashboard\dashboardStyle.css') }}">

<section id = 'indicator' class="content-header">
    <h1>
      Dahboard
      <small>Supervision</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{ route ('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ol>
</section>
<div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
            <!-- style="background-color: #605ca8;color: azure;" -->
                <div class="panel-heading" >
                    CLIENTS SANS IGG/EMAIL
                    <a  data-toggle="collapse" data-target="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><i id = 'minus' class="fa fa-minus"></i></a>
                </div>
            <div id = 'collapseExample'  class = "collapse in">
                <div class="panel-body">
                    <table class="table table-hover" id="incompleteTable">
                        <thead>
                            <tr>
                                <th>Active</th>
                                <th>IGG</th>
                                <th>Nom Complet</th>
                                <th>Email</th>
                                <th>Edition</th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                </div>
            </div>    
        </div>
    </div>
</div>
</div>

<div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" >
                <!-- style="background-color: #605ca8;color: azure;" -->
                    CLIENTS DESACTIVES
                    <a data-toggle="collapse" data-target="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2"><i id = 'minus' class="fa fa-minus"></i></a>
                </div>
                
                    <div id = "collapseExample2" class="collapse in panel-body">
                        <table class="table table-hover" id="inactiveClients">
                            <thead>
                                <tr>
                                    <th>Active</th>
                                    <th>IGG</th>
                                    <th>Nom Complet</th>
                                    <th>Email</th>
                                    <th>Edition</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                   
            </div>
        </div>
    </div>
</div>

@endsection

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script src="{{asset('js\dashboard\dashboard.js')}}"></script>