@extends('layouts.master')

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<link rel="stylesheet" href="{{ asset('css\clients\indexClient.css') }}">

@section('content')

<section id = 'indicator' class="content-header">
        <h1>
          Clients
          <small>Liste des clients</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i>Clients</a></li>
        </ol>
    </section>
    
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Clients</div>
    
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>IGG</th>
                                    <th>Nom Complet</th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $us)
                                    <tr>
                                        <td>{{ $us->id }}</td>
                                        <td>{{ $us->igg }}</td>
                                        <td>{{ $us->name }}</td>
                                        <td>{{ $us->role->role }}</td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

