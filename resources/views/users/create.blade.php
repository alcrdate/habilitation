@extends('layouts.master')
@section('content')
<div class="panel panel-info">`
  @if(Session::has('success'))

	    <div class="alert alert-success">

	        {{ Session::get('success') }}

	        @php

	        Session::forget('success');

	        @endphp

	    </div>

	  @endif

@if(empty($user))
      <div class="panel-heading">ADD USER</div>
      <div class="panel-body">
        <form action="{{ route('users.store')}}" method="post">
          {{ csrf_field() }}
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Complete name</label>


            <input name = "name" type="text" class="form-control" id="inputEmail4" placeholder="fullname" >


            @if ($errors->has('name'))
                	<span class="text-danger">{{ $errors->first('name') }}</span>
            	@endif
          </div>
          <div class="form-group col-md-6">
            <label for="inputPassword4">IGG</label>

            <input name = "igg" type="text" class="form-control" id="inputPassword4" placeholder="IGG number">


            @if ($errors->has('igg'))

                	<span class="text-danger">{{ $errors->first('igg') }}</span>

            @endif
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCity">Email</label>

              <input name = "email" type="text" class="form-control" id="inputCity" placeholder="Email">



            @if ($errors->has('email'))

                	<span class="text-danger">{{ $errors->first('email') }}</span>

            	@endif
          </div>
          <div class="form-group col-md-6">
            <label for="inputState">Role</label>
            <select  name = "role" id="inputState" class="form-control">
              @foreach($role as $r)

                  <option value="{{ $r->id }}" name = "role">{{$r->role}}</option>

              @endforeach
            </select>
          </div>

          <div class="form-group col-md-12">

              <button style="float:right;" type="submit" class="btn btn-primary">ADD</button>

          </div>
        </div>
      </form>
      </div>
</div>
@else

      <div class="panel-heading">Update user
          <a href="{{route('users.create')}}"><img style="max-width: 20px;max-height: 20px;float:right;" src="{{ asset('assets/images/logos/add.png') }}" alt=""></a>
      </div>

      <div class="panel-body">
        <form action="{{ url('users') }}/{{ $user->id }}/edit" method="get">
          {{ csrf_field() }}
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Complete name</label>
            <input name = "name" type="text" class="form-control" id="inputEmail4" placeholder="fullname" value="{{ $user->name }}">
            @if ($errors->has('name'))
                	<span class="text-danger">{{ $errors->first('name') }}</span>
            	@endif
          </div>
          <div class="form-group col-md-6">
            <label for="inputPassword4">IGG</label>

            <input name = "igg" type="text" class="form-control" id="inputPassword4" placeholder="IGG number" value="{{ $user->igg }}">

            @if ($errors->has('igg'))

                	<span class="text-danger">{{ $errors->first('igg') }}</span>

            @endif
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCity">Email</label>

              <input name = "email" type="text" class="form-control" id="inputCity" placeholder="Email" value="{{ $user->email }}">



            @if ($errors->has('email'))

                	<span class="text-danger">{{ $errors->first('email') }}</span>

            	@endif
          </div>
          <div class="form-group col-md-6">
            <label for="inputState">Role</label>
            <select  name = "role" id="inputState" class="form-control">
              <option value="{{$user->role->id}}" selected="{{ $user->role->role }}" > {{ $user->role->role }}</option>
              @foreach($role as $r)
                  <option  value="{{ $r->id }}" name = "role">{{$r->role}}</option>
              @endforeach

            </select>
          </div>

          <div class="form-group col-md-12">


              <button style="float:right;" type="submit" class="btn btn-primary">Update</button

          </div>
        </div>
      </form>
      </div>
</div>
@endif
<div class="panel panel-info">
      <div class="panel-heading">USERS
      </div>
      <div class="panel-body">
        <table class="table table-hover">
        <thead>
          <tr>

            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">IGG</th>
            <th scope="col">Role</th>
            <th scope="col">Todo</th>
          </tr>
        </thead>

        <tbody>

        @foreach ($users_share as $us)
          <tr>
            <td>{{ $us->id }}</td>
            <td>{{ $us->name }}</td>
            <td>{{ $us->igg }}</td>
            <td>{{ $us->role->role }}</td>

            <td >
              <a href="{{ url('users') }}/{{ $us->id }}">
                <button style="float:right;" class = "btn btn-primary" name="button">Update</button>
              </a>

              <form class="" action="{{ route('users.destroy' , $us->id) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                  <button  type = "submit" style="float:right;margin-right:10px;" class = "btn btn-danger" name="button">Delete</button>
              </form>

            </td>
          </tr>
        @endforeach
        </tbody>
        </table>
      </div>
    </div>
@endsection
