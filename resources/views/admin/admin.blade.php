@extends('layouts.master')

<link rel="stylesheet" href="{{ asset('css\clients\indexClient.css') }}">
<link rel="stylesheet" href="{{ asset('css/admin/adminStyle.css') }}">

@section('content')
{{-- Liste des administrateurs --}}

    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Liste des admins
                        <div id = "bar_icons">
                            <a data-toggle="modal" data-target="#adminModal" data-toggle="tooltip" title="Ajouter un admin"><i style = "margin-right:10px;" class="fa fa-plus"></i></a>
                            <a  data-toggle="collapse" data-target="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Minimize"><i class="fa fa-square"></i></a>
                        </div>    
                    </div>
    
                    <div class="panel-body collapse in"  id = 'collapseExample'>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>IGG</th>
                                    <th>Nom Complet</th>
                                    <th>Role</th>
                                    <th style="text-align:center;">Edition</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $us)
                                    <tr>
                                        <td>{{ $us->igg }}</td>
                                        <td>{{ $us->name }}</td>
                                        <td>{{ $us->role->role }}</td>
                                        <td style="text-align:center;">
                                            <a href="#"><button class = "btn btn-primary">Edit</button></a>
                                            <a href="#"><button class = "btn btn-danger">Delete</button></a>
                                        </td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                        {{-- <button style="float:right;background-color:#272F32;" type="submit" class="btn btn-primary" data-toggle="modal" data-target="#adminModal" data-toggle="tooltip">Ajouter</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Liste des lots --}}

    <div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Liste des lots
                        <div id = "bar_icons">
                            <a data-toggle="modal" data-target="#adminModal" data-toggle="tooltip" title="Ajouter un admin"><i style = "margin-right:10px;" class="fa fa-plus"></i></a>
                            <a  data-toggle="collapse" data-target="#collapseLot" role="button" aria-expanded="false" aria-controls="collapseExample" title="Minimize"><i class="fa fa-square"></i></a>
                        </div>
                    </div>
    
                    <div class="panel-body collapse"  id = 'collapseLot'>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Lot</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($lots as $l)
                                    <tr>
                                        <td>{{ $l->id }}</td>
                                        <td>{{ $l->nom }}</td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Liste des RITMS
                        <div id = "bar_icons">
                            <a data-toggle="modal" data-target="#adminModal" data-toggle="tooltip" title="Ajouter un admin"><i style = "margin-right:10px;" class="fa fa-plus"></i></a>
                            <a  data-toggle="collapse" data-target="#collapseRitm" role="button" aria-expanded="false" aria-controls="collapseExample" title="Minimize"><i class="fa fa-square"></i></a>
                        </div>
                    </div>
    
                    <div class="panel-body collapse"  id = 'collapseRitm'>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ritm</th>
                                    <th>Alias</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($ritms as $r)
                                    <tr>
                                        <td>{{ $r->id }}</td>
                                        <td>{{ $r->nom }}</td>
                                        <td>{{ $r->alias }}</td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Liste des applications
                        <div id = "bar_icons">
                            <a data-toggle="modal" data-target="#appModal" data-toggle="tooltip" title="Ajouter un admin"><i style = "margin-right:10px;" class="fa fa-plus"></i></a>
                            <a  data-toggle="collapse" data-target="#collapseApp" role="button" aria-expanded="false" aria-controls="collapseExample" title="Minimize"><i class="fa fa-square"></i></a>
                        </div>
                    </div>
    
                    <div class="panel-body collapse"  id = 'collapseApp'>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Application</th>
                                </tr>
                            </thead>
                            <tbody id = "applications">
                                @foreach($apps as $a)
                                    <tr>
                                        <td>{{ $a->id }}</td>
                                        <td>{{ $a->nom }} <a id = "trash" href=""><i class="fa fa-trash"></i></a><a href=""><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Liste des roles
                            <div id = "bar_icons">
                                <a data-toggle="modal" data-target="#adminModal" data-toggle="tooltip" title="Ajouter un admin"><i style = "margin-right:10px;" class="fa fa-plus"></i></a>
                                <a  data-toggle="collapse" data-target="#collapseRole" role="button" aria-expanded="false" aria-controls="collapseExample" title="Minimize"><i class="fa fa-square"></i></a>
                            </div>
                        </div>
        
                        <div class="panel-body collapse"  id = 'collapseRole'>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($roles as $ro)
                                        <tr>
                                            <td>{{ $ro->id }}</td>
                                            <td>{{ $ro->role }}</td>
                                        </tr>
                                    @endforeach    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection

<div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    {{ csrf_field() }}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Attribuer les droits à : </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('addAdmin') }}" method = "get">
                <div class="modal-body">
                    <input type="text" class="form-control" name = "name" placeholder="Nom d'utilisateur" id = 'clientSearch' autocomplete="off">
                    <div style="margin-top: 15px;">
                        <table class = "table table-hover">
                            <tbody id = 'resultat'>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn" id = "box_header"  value='Ajouter'>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="appModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    {{ csrf_field() }}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter une application : </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method = "get">
                <div class="modal-body">
                    <input type="text" class="form-control" name = "name" placeholder="Nom d'application" id = 'ajouterApplication' autocomplete="off">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn" id = "box_header"  value='Ajouter'>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="{{ asset('js/app.js') }}"></script>

<script>
    $(document).ready(function(){
        
        function get_client_list(name = ''){
            $.ajax({
                url : "{{ route('clientListAjax') }}",
                method : "GET",
                data : {name:name},
                dataType : 'json',
                success:function(output){
                    $('#resultat').html(output);
                }
            })
        }

        get_client_list();

        $(document).on('keyup' , '#clientSearch' , function(){
            var name = $(this).val();
            get_client_list(name);
        });

        $(document).on('click' , '#trname #tdname' , function(){
            $("#clientSearch").val($(this).text());
        });
    });
</script>