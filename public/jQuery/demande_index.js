
  $(document).ready(function(){

    demande_data();

    function demande_data(query = ''){
      $.ajax({
        url : "{{ route('action')}}",
        method : "GET",
        data : {query:query},
        dataType : 'json',
        success : function(demandes){
          $('#tableAjax').html(demandes.table_data);
        }
      })
    }

    $(document).on('keyup' , '#search' , function(){
      var query = $(this).val();
      demande_data(query);
    });

    function btn_status(status = ''){
      $.ajax({  
          url:"{{ route('changerStatus') }}",  
          method:"get",  
          data:{status:status},
          dataType : 'json',  
          success:function(output){  
              $('#success_message').fadeIn().text(output);
              setTimeout(function() {
                $('#success_message').fadeOut("slow");
              }, 2000 );
          }  
      }); 
    }
    //$('button').attr("disabled", "disabled");
    $('button').click(function(){  
        var status = $(this).val();  
        btn_status(status); 
    })

     

  });
