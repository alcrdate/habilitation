$(document).ready(function(){
    $('#DemandeTable').DataTable({
        iDisplayLength : 15,
        processing : true,
        serverSide : true,
        ajax : "./guestGetDemandeTable",
        "columns" : [
            {data : "element"},
            {data : "client_name"},
            {data : "ritm"},
            {data : "ouvert"},
            {data : "statut"}
        ]
    });
});