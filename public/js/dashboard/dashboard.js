        $(document).ready( function () {

            $('#incompleteTable').DataTable({
                "iDisplayLength": 6,
                "processing" : true,
                //"serverSide" : true,
                "ajax": "./getIncompleteClients",
                "columnDefs": [{
                 targets:-1,
                 render: function (data, type, row) {
                     if(type === 'display'){
                        data = '<a href = "./clients/' + data + '" ><span class = "label label-primary">EDIT</span></a>';
                     }
                     return data;
                 }
                },
                {
                 targets:0,
                 render: function (data, type, row) {
                     if(type === 'display'){
                         if(data === 'inactive'){
                            data = '<span class="label label-danger">Inactive</span>';
                         }else{
                            data = '<span class="label label-success">Active</span>';
                         }
                         
                    }
                     return data;
                 }
                }],
                "columns": 
                [
                    { data: "active"},
                    { data: "igg" },
                    { data: "nom_complet" },
                    { data: "email" },
                    { data: "id"},
                ]
            });

            $('#inactiveClients').DataTable({
                "iDisplayLength": 5,
                "processing" : true,
                "serverSide" : true,
                "ajax": "./getInactiveClients",
                "columnDefs": [{
                 targets:-1,
                 render: function (data, type, row) {
                     if(type === 'display'){
                         data = '<a href = "./clients/' + data + '" ><span class = "label label-primary">EDIT</span></a>';
                     }
                     return data;
                 }
                },
                {
                 targets:0,
                 render: function (data, type, row) {
                     if(type === 'display'){
                         if(data === 'inactive'){
                            data = '<span class="label label-danger">Inactive</span>';
                         }else{
                            data = '<span class="label label-success">Active</span>';
                         }
                         
                    }
                     return data;
                 }
                }],
                "columns": 
                [
                    { data: "active"},
                    { data: "igg" },
                    { data: "nom_complet" },
                    { data: "email" },
                    { data: "id"},
                ]
            });

        });
