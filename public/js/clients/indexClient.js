$(document).ready( function () {
    $('#clientsTable').DataTable({
        scrollY: '60vh',
      //scrollCollapse: true,
        "iDisplayLength": 15,
        "processing" : true,
        "serverSide" : true,
        "ajax": "./getClients",
        "columnDefs": [{
            targets:-1,
            render: function (data, type, row) {
                if(type === 'display'){
                    data = '<a href = "./clients/' + data + '"><span class="label label-primary">Edit</span></a>';
                }
                return data;
            }
        },
        {
            targets:0,
            render: function (data, type, row) {
                if(type === 'display'){
                    if(data === 'inactive'){
                    data = '<span class="label label-danger">Inactive</span>';
                    }else{
                    data = '<a href="#" ><span class="label label-success">Active</span></a>';
                    }
                    
            }
                return data;
            }
        },
        {
            targets : -2,
            "visible": false
        }],
        "columns": [
            { data: "active"},
            { data: "igg" },
            { data: "nom_complet" },
            { data: "email" },
            { data : "lot_name"},
            { data: "id"}
        ]
    });
});