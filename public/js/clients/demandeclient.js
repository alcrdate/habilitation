$(document).ready(function(){
    function activate(status = ''){
        $.ajax({
            url:"../activate",
            method: "get",
            data:{status:status},
            dataType:'json',
            success:function(output){  
                $('#success_message').fadeIn("slow").text(output);
            }
        });
    }

    function desactive(status = ''){
        $.ajax({
            url : "../desactive",
            method : "get",
            data : {status:status},
            dataType : 'json',
            success:function(output){
                $('#desactive_message').fadeIn("slow").text(output);
            }
        });
    }
    
    $('#btnActive').click(function(){
        var status = $(this).val();
        activate(status);
    })

    $('#btnDesactive').click(function(){
        var status = $(this).val();
        desactive(status);
    })

});