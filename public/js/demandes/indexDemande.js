
$(document).ready(function(){

    // $('#demandesTable tbody ').on('click', 'td.details-control', function () {
    //     alert('red is clicked');
    //     //$(this).closest('tr').find('button').text()
    // } );


  $('#demandesTable').on('click', '.btn_c', function(){
      var status = $(this).val();  
      btn_status(status); 
  });

  function btn_status(status = ''){
    $.ajax({  
        url:"./changerStatut",  
        method:"get",  
        data:{status:status},
        dataType : 'json',  
        success:function(output){  
          $('#success_message').fadeIn("slow").text(output);
            setTimeout(function() {
              $('#success_message').fadeOut("slow");
            }, 2000 );
        }  
    }); 
  }

  $(document).ready(function(){
  
    $('#demandesTable').DataTable({
      //scrollY: "60vh",
      //scrollCollapse: true,
      "lengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
      "responsive": true,
      "iDisplayLength": 15,
      "processing" : true,
      "serverSide" : true,
      "ajax" : "./getDemandeTable",
      "columnDefs" : [
      {

        targets : -1,
        render : function(data,type,row){
          if( type === 'display'){

            if(row['statut'] == 'Ouvert'){

              data  = 
              '<button title="Ouvert" data-toggle="tooltip" class = "btn_c" value = "Ouvert_' + data + '">' +
                '<i  style = "color:orange;" id = "icons" class="fa fa-circle-o"></i>'+
              '</button>'+
              
            ' <button title="Travail en cours" data-toggle="tooltip" class = "btn_c" value = "Travail en cours_' + data + '">'+
                '<i style = "color:grey;" id = "icons" class="fa fa-spinner"></i>'+
              '</button>' +

              '<button title="Terminé" data-toggle="tooltip" class = "btn_c" value = "Terminé_' + data + '">'+
              ' <i style = "color:grey;" id = "icons" class="fa fa-check-circle"></i>'+
              '</button>'+

              '<button title="Rejeté" data-toggle="tooltip" class = "btn_c" value = "Rejeté_' + data + '">'+
              ' <i style = "color:grey;" id = "icons" class="fa fa-times-circle"></i>'+
            ' </button>';

            }else if(row['statut'] == 'Travail en cours'){

              data  = 
              '<button title="Ouvert" data-toggle="tooltip" class = "btn_c" value = "Ouvert_' + data + '">' +
                '<i  style = "color:grey;" id = "icons" class="fa fa-circle-o"></i>'+
              '</button>'+
              
            ' <button title="Travail en cours" data-toggle="tooltip" class = "btn_c" value = "Travail en cours_' + data + '">'+
                '<i style = "color:blue;" id = "icons" class="fa fa-spinner"></i>'+
              '</button>' +

              '<button title="Terminé" data-toggle="tooltip" class = "btn_c" value = "Terminé_' + data + '">'+
              ' <i style = "color:grey;" id = "icons" class="fa fa-check-circle"></i>'+
              '</button>'+

              '<button title="Rejeté" data-toggle="tooltip" class = "btn_c" value = "Rejeté_' + data + '">'+
              ' <i style = "color:grey;" id = "icons" class="fa fa-times-circle"></i>'+
            ' </button>';

            }else if(row['statut'] == 'Terminé'){

              data  = 
              '<button title="Ouvert" data-toggle="tooltip" class = "btn_c" value = "Ouvert_' + data + '">' +
                '<i  style = "color:grey;" id = "icons" class="fa fa-circle-o"></i>'+
              '</button>'+
              
            ' <button title="Travail en cours" data-toggle="tooltip" class = "btn_c" value = "Travail en cours_' + data + '">'+
                '<i style = "color:grey;" id = "icons" class="fa fa-spinner"></i>'+
              '</button>' +

              '<button title="Terminé" data-toggle="tooltip" class = "btn_c" value = "Terminé_' + data + '">'+
              ' <i style = "color:green;" id = "icons" class="fa fa-check-circle"></i>'+
              '</button>'+

              '<button title="Rejeté" data-toggle="tooltip" class = "btn_c" value = "Rejeté_' + data + '">'+
              ' <i style = "color:grey;" id = "icons" class="fa fa-times-circle"></i>'+
            ' </button>';

            }else{
                
              data  = '<button title="Ouvert" data-toggle="tooltip" class = "btn_c" value = "Ouvert_' + data + '">' +
                '<i  style = "color:grey;" id = "icons" class="fa fa-circle-o"></i>'+
              '</button>'+
              
            ' <button title="Travail en cours" data-toggle="tooltip" class = "btn_c" value = "Travail en cours_' + data + '">'+
                '<i style = "color:grey;" id = "icons" class="fa fa-spinner"></i>'+
              '</button>' +

              '<button title="Terminé" data-toggle="tooltip" class = "btn_c" value = "Terminé_' + data + '">'+
              ' <i style = "color:grey;" id = "icons" class="fa fa-check-circle"></i>'+
              '</button>'+

              '<button title="Rejeté" data-toggle="tooltip" class = "btn_c" value = "Rejeté_' + data + '">'+
              ' <i style = "color:red;" id = "icons" class="fa fa-times-circle"></i>'+
            ' </button>';
            }
            
          }
          return data;
        }
      },
      {
        targets : 0,
        render: function ( data, type, row ) {
          return data.length > 70 ?
              data.substr( 0, 70 ) +'…' :
              data;
        }
      }, 
      // {
      //   targets:5,
      //   //"visible": false,
      //   render : function(data,type,row){
      //     if(type === 'display'){
      //       if(data === 'Ouvert'){
      //           data = '<span class="label label-warning">----- Ouvert -----</span>';
      //         }else if(data === 'Travail en cours'){
      //           data = '<a href = "' + data + '" ><span class="label label-primary">Travail en cours</span></a>';
      //         }else if(data === 'Rejeté'){
      //           data = '<span class="label label-danger">----- Rejeté ------</span>';
      //         }else{
      //           data = '<span class="label label-success">---- Terminé ----</span>';
      //         }
      //       }
      //     return data;
      //   }
      // },
      ],
      "columns" : [
        { 
          "data" : "element", 
          "className" : "dtails-control"
        },
        { "data" : "client_name" },
        { "data" : "ritm" },
        { "data" : "ouvert"},
        //{ "data" : "livraison"},
        //{ "data" : "statut"},
        { "data" : "id" }
      ],
      "order": [[3, 'des']]
    });

  });

});


function copy() {
  var copyText = document.getElementById("mail");
  copyText.select();
  document.execCommand("copy");
}