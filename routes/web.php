<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*------------------- All tests are here ------------*/
Route::get('/test' , 'TestController@test');

/*-- Beta functions --*/
Route::get('/logout' , function(){
    return ('home');
});

Route::get('/home', function(){
    return view('guest.demandes.index');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*------------------- Guest --------------------------*/
Route::get('/demandes' , function(){
    return view('guest.demandes.index');
})->name('guestDemandesIndex');

Route::get('/clients' , function(){
    return view('guest.clients.index');
})->name('guestClientsIndex');

Route::get('/guestGetDemandeTable ', 'GuestDemandeController@guestDemandeTable')->name('guestDemandeTable');
Route::get('/guestGetClientsTable' , 'GuestDemandeController@guestClientTable')->name('guestClientTable');
Route::get('/clients/{client}' , 'GuestDemandeController@show');
Route::get('/guest/export' , 'GuestDemandeController@downloadExcel')->name('exportGuest');


/*------------------- User ---------------------------*/
Route::resource('users', 'UserController');
Route::get('/dashboard/manage' , 'Usercontroller@index')->middleware('admin');
Route::get('/dashboard/manage/create' , 'UserController@create')->middleware('admin');

Route::get('/admin' , 'AdminController@index')->name('admin');
Route::get('/admin/getClientList' , 'AdminController@searchClient')->name('clientListAjax');
Route::get('/admin/add' , 'AdminController@add')->name('addAdmin');


/*-------------------- Groupe Admin -----------------*/
Route::group(['prefix' => 'admin' , 'middleware' => 'admin'] , function(){

    Route::get('/export' , 'DemandeController@downloadExcel')->name('export');
    /*----------------- Dashboard -----------------------*/
    Route::get('dashboard' , 'DashboardController@index')->name('dashboard');
    
    
    /*----------------- Demande -------------------------*/
    Route::resource('demandes' , 'DemandeController');
    Route::get('/old' , 'DemandeController@old');
    Route::post('/demandes/scrap' , 'DemandeController@scrap')->name('scrap');
    Route::get('/changerStatut' , 'DemandeController@changerStatus')->name('changerStatus');
    Route::get('/getDemandes' , 'DemandeController@getDemandes')->name('getDemandes');
    Route::get('/getDemandeTable' , 'DemandeController@demandeTable')->name('demandeTable');

    /*----------------- Client -------------------------*/
    Route::resource('clients' , 'ClientController');
    Route::get('/getClients' , 'ClientController@getClients')->name('dtable');
    Route::get('/getIncompleteClients' , 'ClientController@getIncompleteClients')->name('incompleteClients');
    Route::get('/getInactiveClients' , 'ClientController@getInactiveClients')->name('inactiveClients');
    Route::get('/activate' , 'ClientController@activate')->name('activate');
    Route::get('/desactive' , 'ClientController@desactivate')->name('desactivate');

    /*------------------ Incidents ---------------------*/
    Route::get('/incidents' , 'IncidentController@index')->name('incidents.index');

});



/*----------------- Ajax --------------------------*/
Route::get('ajax',function(){
    return view('test');
});
Route::get('/search','TestController@action')->name('action');
Route::get('/searchRadio' , 'TestController@actionRadio')->name('actionRadio');