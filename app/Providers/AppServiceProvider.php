<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use App\User;
// use App\Role;
// use App\Permission;
// use App\Menu;

use App\Client;
use View;
use App\Iettable;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $iclients = Client::whereNull('igg')->orWhereNull('email')->get();
        $iettable = Iettable::where('statut', '=' , '0')->get();

        view()->share('iclients', $iclients);
        view()->share('iettable', $iettable);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
