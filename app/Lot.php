<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lot extends Model
{
    protected $fillable = [
        'nom'
    ];

    public function applications_lot(){
        return $this->belongsToMany('App\Application');
    }
}
