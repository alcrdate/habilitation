<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'nom_complet', 
        'nom',
        'prenom',
        'email',
        'igg',
        'role_id',
        'lot_id',
        'lot_name'
    ];

    protected $hidden = [
        'password'
    ];

    public function demande(){
        return $this->hasMany('App\Demande');
    }

    public function applications(){
        return $this->belongsToMany('App\Application');
    }

    public function lot(){
        return $this->belongsTo('App\Lot');
    }
}
