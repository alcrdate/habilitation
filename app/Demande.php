<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    protected $fillable = [
        'client_id',
        'client_name',
        'demandeur_name',
        'ritm',
        'element',
        'ouvert',
        'statut',
        'livraison'
    ];

    public function client(){
        return $this->BelongsTo('App\Client');
    }
}
