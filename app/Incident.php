<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    protected $fillable = [
        'client_id',
        'client_name',
        'point_de_contact',
        'inc',
        'element',
        'ouvert',
        'statut',
        'ferme'
    ];

    public function client(){
        return $this->belongsTo('App\Client');
    }
}
