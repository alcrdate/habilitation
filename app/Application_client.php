<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application_client extends Model
{
	protected $table = 'application_client';
	protected $fillable = ['client_id' , 'application_id'];
}
