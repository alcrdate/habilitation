<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'nom'
    ];

    public function clients(){
        return $this->belongsToMany('App\Client');
    }

    public function lots(){
        return $this->belongsToMany('App\Lot');
    }
}
