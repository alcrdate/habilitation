<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ritm extends Model
{
    protected $fillable = ['id' , 'nom' , 'alias'];
}
