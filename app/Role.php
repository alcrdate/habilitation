<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return  $this->belongsToMany('App\User');
    }

    public function permissions(){
        return $this->belongsToMany('App\Permission');
    }

    public function menus(){
      return $this->belongsToMany('App\Menu');
    }

    public function user(){
      return $this->belongsTo('App\User');
    }
}
