<?php

namespace App\Http\Controllers;


use App\User;
use App\Permission;
use App\Role;
use App\Menu;
use View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Demande;
use Response;


class TestController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index(){
      $users = User::find(Auth::user()->id);
      $roles = Role::all();
      $permissions = Permission::all();
      $usr = User::all();
      $menus = Menu::all();

      
      // foreach ($roles as $r) {
      //     foreach ($r->permissions as $p) {
      //         echo $r->role . " = " . $p->permission . "<br>";
      //     }
      // }



      return View::make('home')
      ->with('users',$users)
      ->with('roles' , $roles)
      ->with('permissions' , $permissions)
      ->with('menus' , $menus);
      }

      public function test(){
        echo "List of Demandes <br> ------------------- <br>";
        $demandes = Demande::all();
        foreach ($demandes as $demande) {
          echo "client_name (Demande_table) : " . $demande->client_name . "<br>";
          echo "client_id : " . $demande->client_id . "<br>";
          echo $demande->client->email . "<br>";
          echo "----------------------------------- <br>";
          break;
        }
      
        echo "<strong>search : </strong> <br>";
        $michel = Demande::where('client_name' , 'like' , '%alaoui%')->where('ritm' , 'like' , '%%')->get();
        dd($michel);
        foreach ($michel as $m) {
          echo "Client name : " . $m->client_name . "<br>"; 
          echo "Client ritm : " . $m->ritm . "<br>";
          echo "----------------------------------- <br>"; 
        }
      }

      public function actionRadio(Request $request){
        if($request->ajax()){
          $output = 'Nada'; 
          $g = $request->get('gender');
          $output = $g;
          echo json_encode($output);
        }
      }
      
      public function statutBar($statut){
        $bar = '<td id = "statusBtn" >';
        $ss = explode(',' , $statut);
        foreach($ss as $s){
          if($s == 'Ouvert'){
            $bar .= '
              
            <button id = "btn" type="button" class="btn btn-box-tool" title="Ouvert" data-toggle="tooltip" value = "Ouvert_{{$d->id}}">
              <i  style = "color:green;" id = "icons" class="fa fa-circle-o"></i>
            </button>
            
            <button id = "btn" type="button" class="btn btn-box-tool" title="Travail en cours" data-toggle="tooltip" value = "Travail en cours_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-spinner"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Fermé terminé" data-toggle="tooltip" value = "Terminé_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-check-circle"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Rejeté" data-toggle="tooltip" value = "Rejeté_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-times-circle"></i>
            </button>
            
          <br>';
          }elseif($s == 'Travail en cours'){
            $bar .= '
              
            <button id = "btn" type="button" class="btn btn-box-tool" title="Ouvert" data-toggle="tooltip" value = "Ouvert_{{$d->id}}">
              <i  style = "color:gery;" id = "icons" class="fa fa-circle-o"></i>
            </button>
            
            <button id = "btn" type="button" class="btn btn-box-tool" title="Travail en cours" data-toggle="tooltip" value = "Travail en cours_{{$d->id}}">
              <i style = "color:blue;" id = "icons" class="fa fa-spinner"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Fermé terminé" data-toggle="tooltip" value = "Terminé_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-check-circle"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Rejeté" data-toggle="tooltip" value = "Rejeté_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-times-circle"></i>
            </button>
            
            <br>';
          }elseif($s == 'Terminé'){
            $bar .= '
              
            <button id = "btn" type="button" class="btn btn-box-tool" title="Ouvert" data-toggle="tooltip" value = "Ouvert_{{$d->id}}">
              <i  style = "color:gery;" id = "icons" class="fa fa-circle-o"></i>
            </button>
            
            <button id = "btn" type="button" class="btn btn-box-tool" title="Travail en cours" data-toggle="tooltip" value = "Travail en cours_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-spinner"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Fermé terminé" data-toggle="tooltip" value = "Terminé_{{$d->id}}">
              <i style = "color:green;" id = "icons" class="fa fa-check-circle"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Rejeté" data-toggle="tooltip" value = "Rejeté_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-times-circle"></i>
            </button>
            
            <br>';
          }elseif($s == 'Rejeté'){
            $bar .= '
              
            <button id = "btn" type="button" class="btn btn-box-tool" title="Ouvert" data-toggle="tooltip" value = "Ouvert_{{$d->id}}">
              <i  style = "color:gery;" id = "icons" class="fa fa-circle-o"></i>
            </button>
            
            <button id = "btn" type="button" class="btn btn-box-tool" title="Travail en cours" data-toggle="tooltip" value = "Travail en cours_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-spinner"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Fermé terminé" data-toggle="tooltip" value = "Terminé_{{$d->id}}">
              <i style = "color:gery;" id = "icons" class="fa fa-check-circle"></i>
            </button>

            <button id = "btn" type="button" class="btn btn-box-tool" title="Rejeté" data-toggle="tooltip" value = "Rejeté_{{$d->id}}">
              <i style = "color:red;" id = "icons" class="fa fa-times-circle"></i>
            </button>
            <br>';
          }else{
            $bar .= '</td>';
          }
          
        }
        return $bar;
      }

      public function action(Request $request){
        
        if($request->ajax()){
          $output = '';
          $query = $request->get('query');
          if($query != ''){
            $demandes = Demande::where('client_name' , 'like' , '%' .$query. '%')->orWhere('ritm' , 'like' , '%'. $query .'%')->get();
          }else{
            $demandes = Demande::paginate(7);
          }
          $total_row = $demandes->count();
          if($total_row > 0){
            $bar = '';
            foreach($demandes as $d){
              $bar .= $d->statut . ',';
              
              $output .= '
              <tr>
                <td><a href=#> ' . $d->ritm . '</a></td>
                <td> ' . $d->client_name . '</td>
                <td> ' . $d->element . '</td>
                <td> ' . $d->ouvert . '</td>
                <td> ' . $d->livraison . '</td>
                <td> ' . $d->statut . '</td>
              </tr>
              '; 
            }
          }else{
            $output = '
              <tr>
                <td> No Data found </td>
              </tr>
            ';
          }
          $statut = $this->statutBar($bar);
          $demandes = array(
            'statut' => $statut,
            'table_data'  => $output,
            'total_data'  => $total_row
          );

          echo json_encode($demandes);
        }
      }


  }  
