<?php

namespace App\Http\Controllers;
use SebastianBergmann\Environment\Console;
use Illuminate\Http\Request;
use Illuminate\View\View;

use App\User;
use App\Lot;
use App\Ritm;
use App\Role;
use App\Client;
use App\Demande;
use Excel;
use App\Application;

class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin'); 
    }

    public function index(){
        $ritms = Ritm::all();
        $lots = Lot::all();
        $users = User::all();
        $roles = Role::all();
        $apps = Application::all();

        return view('admin.admin')
            ->with('apps' , $apps)
            ->with('users' , $users)
            ->with('ritms' , $ritms)
            ->with('roles' , $roles)
            ->with('lots' , $lots);
    }

    public function searchClient(Request $request){
        if($request->ajax()){
            $output = '';
            $query = $request->get('name');
            if($query != ''){
                $client = Client::where('nom_complet' , 'like' , '%' . $query . '%')->orWhere('igg' , 'like' , '%' . $query . '%')->paginate(3);
            }else{
                //$client = Client::paginate(3);
            }
            $total_row  = $client->count();
            if($total_row > 0){
                foreach($client as $c){
                    $output .= '<tr id = "trname"><td style = "width:10px;" >' . $c->igg . '</td><td id="tdname">'. $c->nom_complet . '</td></tr>'; 
                }
            }else if($total_row == 1){
                $name_exact = $c->nom_complet;
            }else{
                $output = 'Aucun resultat';
            }
            
            echo json_encode($output);
        }
    }

    public function add(Request $request){
        $client = Client::where('nom_complet' , 'like' , '%' . $request->name . '%')->get();
        
        $username = "";
        $igg = "";
        $email = "";

        foreach ($client as $c) {
           $username = $c->nom_complet;
           $igg = $c->igg;
           $email = $c->email;
        }

        $user = New User;
        $user->name = $username;
        $user->igg = $igg;
        $user->email = $email;
        $user->password = bcrypt('admin123');
        $user->role_id = 1;
        $user->save();
        
        return redirect()->back();
    }   

    public function export_xls(){
        $demande = Demande::all();

        $data = "";

        if(count($demande) > 0){
            $data .= '<table border = 1>
                <thead>
                    <tr>
                        <td>Demande</td>
                        <td>RITM</td>
                        <td>Client</td>
                        <td>Ouvert</td>
                    </tr>
                </thead>
                <tbody>';
            foreach($demande as $d){
                $data .= '<tr><td>' . $d->element . '</td>
                          <td>' . $d->ritm . '</td>
                          <td>' . $d->client_name . '</td>
                          <td>' . $d->ouvert . '</td></tr>';
            }
                $data .= '</tbody></table>';
        }

        header('Content-Type: application/xls; charset=utf-8');
        header('Content-Disposition: attachement; filename = demande.xls');
        echo $data;
        
    }

    public function deleteUser(Request $req){
        
    }

}
