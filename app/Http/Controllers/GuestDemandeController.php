<?php

namespace App\Http\Controllers;



use App\Demande;
use App\Client;
use App\Lot;
use View;
use Excel;

class GuestDemandeController extends Controller
{
    
    public function guestDemandeTable(){
        $q = Demande::orderBy('ouvert', 'desc');
        return datatables($q)->make(true);
    }

    public function guestClientTable(){
        $q = Client::orderBy('nom_complet' , 'asc');
        return datatables($q)->make(true);
    }

    public function show(Client $client)
    {
        $lots = Lot::all();
        $lot = Lot::find($client->lot_id);
        return View::make('guest.clients.guestDemandeClient')->with('client' , $client)
                                                            ->with('lot' , $lot)
                                                            ->with('lots',$lots);
    }

    public function downloadExcel(){
        $data = Demande::select('element' , 'ritm' , 'client_name' , 'ouvert' , 'statut')->get();
        return Excel::create('Fichier de suivi', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }
}

