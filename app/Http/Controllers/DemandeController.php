<?php

namespace App\Http\Controllers;

use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use View;
use Auth;
use App\Demande;
use App\Client;
use App\Application_client;
use App\Application;
use Excel;
use App\Iettable;

class DemandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin');
    }
     
    public function index()
    {
        $demandes = Demande::orderBy('created_at', 'desc')->paginate(7);
        return View::make('demandes.index')->with('demandes' , $demandes);
    }

    public function old(){
        $demandes = Demande::orderBy('created_at', 'desc')->paginate(7);
        return View::make('demandes.old')->with('demandes' , $demandes);
    }

    public function getDemandes(){
        $q = Demande::select(
                "element", 
                "client_name",
                "ritm",
                "ouvert",
                "livraison",
                "statut"
                );
        return datatables($q)->make(true);
    }

    public function demandeTable(){
        //$q = Demande::all();
        $q = Demande::all();
        return datatables($q)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('demandes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $validateFields = array(
            'client_name' => 'required',
            'demandeur_name' => 'required',
            'ritm' => 'required',
            'element' => 'required',
            'ouvert' => 'required',
            'statut' => 'required'
        );

        $this->validate(request(), $validateFields);

        $id_client = -1;
        $t = explode(" " , $request->client_name);
        $client_email = strtolower($t[0]) . "." . strtolower($t[1]) . "@cgi.com"; 
        if(sizeof($t) == 3){
            $client = Client::where('nom_complet' , 'like' , '%' . $t[0] . '%')->where('nom_complet' , 'like' , '%' . $t[1] . '%')->where('nom_complet' , 'like' , '%' . $t[2] . '%')->get();
        }else{
            $client = Client::where('nom_complet' , 'like' , '%' . $t[0] . '%')->where('nom_complet' , 'like' , '%' . $t[1] . '%')->get();
        }
        $lot_name = 'pas de lot';
        $lot_id = 11;

        foreach($client as $c){
            $id_client = $c->id;
        }

        if($id_client == -1){
            $new_client = Client::create([
                'nom_complet' => $request->client_name,
                'email' => $client_email,
                'lot_name' => $lot_name,
                'lot_id' => $lot_id,
            ]);
            $id_client = $new_client->id;
        }

        

        $demande = Demande::firstOrCreate([
            'ritm' => $request->ritm
        ],[
            'client_id' => $id_client,
            'client_name' => $request->client_name,
            'demandeur_name' => $request->demandeur_name,
            'ritm' => $request->ritm,
            'element' => $request->element,
            'ouvert' => $request->ouvert,
            'statut' => $request->statut,
            'livraison' => $request->livraison
        ]);

      

        //Cas demande creation d'IGG
        if($demande->element == "[MOB] Création / Modification d'un IGG Externe (IET)"){
            $iettable = Iettable::create([
                'element' => $demande->element,
                'ritm' => $demande->ritm,
                'client_name' => $demande->client_name,
                'client_id' => $demande->client_id
            ]);
        }

        //Cas demande creation VM Tara
        if($demande->element == "TARA - Ajout ou Suppression d'utilisateur(s) à une population existante"){
            $iettable = Iettable::where('client_id' , '=' , $demande->client_id)->get();
            if(count($iettable) > 0){
                foreach ($iettable as $iet) {
                    $iet->statut = 1;
                    $iet->save();
                }
            }
        }

        //Cas demande d'installation 
        if($request->outil1 != null){

            if($request->outil2 == ""){
                
                $apps = Application_client::create([
                    'application_id' => $request->outil1,
                    'client_id' => $id_client
                ]);
                
            }else if($request->outil3 == ""){

                $app1 = new Application_client;
                $app2 = new Application_client;

                $app1->application_id = $request->outil1;
                $app1->client_id = $id_client;
                
                $app2->application_id = $request->outil2;
                $app2->client_id = $id_client;

                $app1->save();
                $app2->save();


            }else if($request->outil4 == ""){

                $app1 = new Application_client;
                $app2 = new Application_client;
                $app3 = new Application_client;

                $app1->application_id = $request->outil1;
                $app1->client_id = $id_client;
                
                $app2->application_id = $request->outil2;
                $app2->client_id = $id_client;

                $app3->application_id = $request->outil3;
                $app3->client_id = $id_client;

                $app1->save();
                $app2->save();
                $app3->save();

            }else if($request->outil4 == ""){

                $app1 = new Application_client;
                $app2 = new Application_client;
                $app3 = new Application_client;
                $app4 = new Application_client;

                $app1->application_id = $request->outil1;
                $app1->client_id = $id_client;
                
                $app2->application_id = $request->outil2;
                $app2->client_id = $id_client;

                $app3->application_id = $request->outil3;
                $app3->client_id = $id_client;

                $app4->application_id = $request->outil4;
                $app4->client_id = $id_client;

                $app1->save();
                $app2->save();
                $app3->save();
                $app4->save();

            }else{

                $app1 = new Application_client;
                $app2 = new Application_client;
                $app3 = new Application_client;
                $app4 = new Application_client;
                $app5 = new Application_client;

                $app1->application_id = $request->outil1;
                $app1->client_id = $id_client;
                
                $app2->application_id = $request->outil2;
                $app2->client_id = $id_client;

                $app3->application_id = $request->outil3;
                $app3->client_id = $id_client;

                $app4->application_id = $request->outil4;
                $app4->client_id = $id_client;

                $app5->application_id = $request->outil5;
                $app5->client_id = $id_client;

                $app1->save();
                $app2->save();
                $app3->save();
                $app4->save();
                $app5->save();
            } 
                
        }

        if($demande->wasRecentlyCreated){
            return redirect(route('demandes.index'))->with('success' , 'Demande enregistrée avec succès');
        }else{
            return redirect(route('demandes.index'))->with('already' , 'La demande est déjà enregistrée');
        }
        
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function scrap(Request $req){
        if(empty($req->textS)){
            return back();
        }else{

            $html = HtmlDomParser::str_get_html($req->textS);
            $ritm = $html->find('a[class=linked formlink]');
            foreach ($ritm as $r) {
                echo $r . '<br>';
            }
        }
            /*
            $t = [];

            $ritm = $html->find('input[id=sys_readonly.sc_req_item.number]');
            $elements = $html->find('input[id=sys_display.sc_req_item.cat_item]');
            $client = $html->find('input[id=sys_display.sc_req_item.request.requested_for]');
            $delivery = $html->find('input[id=sc_req_item.estimated_delivery]');
            $state = $html->find('select[id=sc_req_item.state]');
            $requester = $html->find('input[id=sc_req_item.opened_by_label]');
            $ouvert = $html->find('input[id=sys_readonly.sc_req_item.opened_at]');
            $livraison = $html->find('input[id=sc_req_item.estimated_delivery]');

            foreach ($state as $s) {
                foreach ($s->find('option[selected=SELECTED]') as $o) {
                    $value = $o->attr['value'];
                    switch ($value) {
                        case 0:
                            $t['s'] = 'En attente';
                            break;
                        case 1:
                            $t['s'] = 'Ouvert';
                            break;
                        case 2;
                            $t['s'] = 'Travail en cours';
                            break;    
                        case 3:
                            $t['s'] = 'Fermé terminé';
                            break;
                        case 4:
                            $t['s'] = 'Staut numero 4';
                            break;   
                        default:
                            $t['s'] = 'Statut inconnu';
                            break;
                    }    		
                }
            }

            foreach ($elements as $e) {
                $t['e'] = $e->attr['value']; 
            }
            
            foreach ($ouvert as $o){
                $t['o'] = $o->attr['value'];
            }

            foreach($livraison as $l){
                $t['l'] = $l->attr['value'];
            }

            foreach ($ritm as $r) {
                $t['r'] = $r->attr['value'];
            }

            foreach ($client as $c) {
                $t['c'] = $c->attr['value'];
            }

            foreach ($delivery as $d) {
                $t['d'] = $d->attr['value'];
            }

            foreach ($requester as $req) {
                $t['req'] = $req->attr['value'];
            }
            $prenom = explode(" " , $t['c']);  
            $email = "Bonjour " . $prenom[0] . ",\n\nLa demande " . $t['r'] . " est en cours de traitement.";
            $outils = Application::all();
            $t['m'] = $email;
            $t['outils'] = $outils;
            return Redirect::route('demandes.create')->with('t', $t);
*/
        }

    public function changerStatus(Request $request){
        if($request->ajax()){
          $statut = $request->get('status');
          $t_statut = explode('_' , $statut);
          $change_statut = Demande::findOrFail($t_statut[1])->update(['statut'=>$t_statut[0]]);
          $output = 'Staut est : ' . $t_statut[0]; 
          echo json_encode($output);
        }
    }

        
    public function downloadExcel(){
        $data = Demande::select('element' , 'ritm' , 'client_name' , 'ouvert' , 'statut')->get();
        return Excel::create('Fichier de suivi', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }
}

