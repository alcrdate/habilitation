<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Redirect;
use Session;
use Auth;
use View;
use App\Role;

class UserController extends Controller
{

    public function __construct(){
      $this->middleware('admin');
      $this->middleware('auth');
    }

    public function index(){
      $users = User::all();
      return View::make('users.index')->with('users' , $users);
    }

    public function create(){
      $user = User::all();
      $role = Role::all();
      return View::make('users.create')->with('user' , $user)->with('role' , $role);
    }

    public function store(){
      $validateFields = array(
        'name' => 'required',
        'igg' => 'required',
        'email' => 'required|email'
      );

      $this->validate(request(), $validateFields);

      $user = User::create([
          'name' => request()->name,
          'email' => request()->email,
          'igg' => request()->igg,
          'role_id' => request()->role,
          'password' => 'admin123'
      ]);


      return back()->with('success' , 'User created successfully');
    }

    public function show($id){
      $user = User::find($id);

      return View::make('users.create')->with('user' , $user);
    }

    public function edit($id){
      $validateFields = array(
        'name' => 'required',
        'igg' => 'required',
        'email' => 'required|email'
      );

      $this->validate(request(), $validateFields);

      $user = User::find($id);


      $user->name = request()->name;
      $user->email = request()->email;
      $user->igg = request()->igg;
      $user->role_id = request()->role;

      $user->save();

      Session::flash('success', 'The user updated successfully');
      return Redirect::to(route('users.create'));
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();

        Session::flash('success', 'Successfully deleted the user!');
        return Redirect::to(route('users.create'));
    }

}
