<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Redirect;
use View;
use App\Lot;

class ClientController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return View::make('clients.index')->with('clients' , $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $lots = Lot::all();
        $lot = Lot::find($client->lot_id);
        return View::make('clients.demandeClient')->with('client' , $client)
                                                  ->with('lot' , $lot)
                                                  ->with('lots',$lots);
    }


    public function getIncompleteClients(){
        $client = Client::whereNull('igg')->orWhereNull('email');
        return datatables($client)->make(true);
    }


    public function getClients(){
        //$q = Client::select('id' , 'nom_complet' , 'email' , 'igg' , 'active');
        $q = Client::orderBy('nom_complet' , 'asc');
        return datatables($q)->make(true);
    }

    public function getInactiveClients(){
        $client = Client::where('active' , 'like' , '%inactive%');
        return datatables($client)->make(true);
    }

    public function activate(Request $req){
        if ($req->ajax()) {
            $id_client = $req->get('status');
            $client = Client::find($id_client);
            $client->active = 'active';
            $client->save();
            $output = 'le client ' . $client->nom_complet . ' est activé maintenant !';
            echo json_encode($output);
        }
    }

    public function desactivate(Request $req){
        if($req->ajax()){
            $id_client = $req->get('status');
            $client = Client::find($id_client);
            $client->active = 'inactive';
            $client->save();
            $output = 'le client ' . $client->nom_complet . ' est desactivé maintenant !'; 
            echo json_encode($output);
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req, Client $client)
    {
    
        $validateFields = array(
            'nom_complet' => 'max:100',
            'igg' => 'max:20',
            'email' => '',
            'al' => 'max:20',
            'vm' => 'max:100' 
        ); 

        $this->validate(request(), $validateFields);
        
        $new_client = Client::find($client->id);
        $new_client->nom_complet = $req->nom_complet;
        $new_client->igg = $req->igg;
        $new_client->email = $req->email;
        $new_client->al = $req->al;
        $new_client->vm = $req->vm;
        $new_client->net = $req->internet;
        $new_client->admin = $req->admin;
        //$new_client->lot_name = $req->lot;
        $new_client->badge_total = $req->badge;
        $new_client->lot_id = $req->lot;

        $new_client->save();

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

}
