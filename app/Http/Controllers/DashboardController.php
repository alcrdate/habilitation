<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct(){
      $this->middleware('auth');
    }

    public function index(){
      return view('dashboard');
    }
}


// return View::make('dashboard')
// ->with('users',$users)
// ->with('roles' , $roles)
// ->with('permissions' , $permissions)
// ->with('menus' , $menus);
