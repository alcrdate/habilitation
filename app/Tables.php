<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Table extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'id', 'nbLine', 'nbColonne',
    ];

    protected $hidden = [
        'nbLine', 'nbColonne',
    ];
}
?>