<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iettable extends Model
{
    protected $fillable = [
        'element',
        'ritm',
        'client_name',
        'client_id',
        'statut'
    ];
}
