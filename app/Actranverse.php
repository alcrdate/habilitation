<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actranverse extends Model
{
    protected $fillable = [
        'nom'
    ];

    public function clients(){
        return $this->belongsToManu('App\Client');
    }
}
